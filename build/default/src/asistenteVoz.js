const message = {
  "greatLogin": " ,te registraste exitosamente. Revisa tu correo y entra con tu usuario.",
  "errorForm": "Llena el formulario correctamente.",
  "existClient": "El correo con el que quieres registrarte, ya existe",
  "errorLogin": "Parece que introdujiste tus datos de logueo mal.",
  "bienvenida": "Bienvenido Alex.",
  "despedida": "Adios Alex",
  "frace": "La vida es cómo una cámara: enfócate sólo en lo importante, captura los buenos momentos, saca de lo negativo algo positivo, y si no sale como esperabas, intenta una nueva toma.",
  "sabiasque": "Sabías que...El primer partido de básquetbol de la historia, se jugó con un balón de fútbol y dos canastas de frutas a cada lado del gimnasio como canastas."
};

function talk(text) {
  const msg = new SpeechSynthesisUtterance(text);
  msg.rate = 1;
  msg.volume = 0.5;
  msg.lang = 'es-MX';
  window.speechSynthesis.speak(msg);
}