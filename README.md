﻿# Proyecto Final Front Tech U 2020


1) Clonar proyecto
 
```bash
git clone https://Al3jandr0LR@bitbucket.org/Al3jandr0LR/proyectofinalfront.git
```

2) Entrar a la carpeta "proyectofinalfront"
 
```bash
cd proyectofinalfront
```

3) Descargar dependencias bower
 
```bash
bower install
```

4) Construir proyecto dockerizado
 
```bash
docker build -t proyectofinalfront .
```

5) Correr proyecto dockerizado
 
```bash
docker run -d -p 3000:3000 --name proyectofinalfront proyectofinalfront
```

6) Iniciar proyecto en un navegador web
 
[http://localhost:3000/](http://localhost:3000/)


